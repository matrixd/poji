
document.getElementById("i1").oninput = function() {
    var val = document.getElementById("i1").value;
    proc(1,1,val);
    changeStr(1,val);
}

document.getElementById("i2").oninput = function() {
    var val = document.getElementById("i2").value;
    proc(1,2,val);
    changeStr(2,val);
}

document.getElementById("i3").oninput = function() {
    var val = document.getElementById("i3").value;
    proc(1,3,val);
    changeStr(3,val);
}

document.getElementById("i4").oninput = function() {
    var val = document.getElementById("i4").value;
    proc(1,4,val);
    changeStr(4,val);
}

document.getElementById("i5").oninput = function() {
    var val = document.getElementById("i5").value;
    proc(2,1,val);
    changeStr(5,val);
}

document.getElementById("i6").oninput = function() {
    var val = document.getElementById("i6").value;
    proc(2,2,val);
    changeStr(6,val);
}

document.getElementById("i7").oninput = function() {
    var val = document.getElementById("i7").value;
    proc(2,3,val);
    changeStr(7,val);
}

document.getElementById("i8").oninput = function() {
    var val = document.getElementById("i8").value;
    proc(2,4,val);
    changeStr(8,val);
}

document.getElementById("i9").oninput = function() {
    var val = document.getElementById("i9").value;
    proc(3,1,val);
    changeStr(9,val);
}

document.getElementById("i10").oninput = function() {
    var val = document.getElementById("i10").value;
    proc(3,2,val);
    changeStr(10,val);
}

document.getElementById("i11").oninput = function() {
    var val = document.getElementById("i11").value;
    proc(3,3,val);
    changeStr(11,val);
}

document.getElementById("i12").oninput = function() {
    var val = document.getElementById("i12").value;
    proc(3,4,val);
    changeStr(12,val);
}

document.getElementById("i13").oninput = function() {
    var val = document.getElementById("i13").value;
    proc(4,1,val);
    changeStr(13,val);
}

document.getElementById("i14").oninput = function() {
    var val = document.getElementById("i14").value;
    proc(4,2,val);
    changeStr(14,val);
}

document.getElementById("i15").oninput = function() {
    var val = document.getElementById("i15").value
    proc(4,3,val);
    changeStr(15,val);
}

document.getElementById("i16").oninput = function() {
    var val = document.getElementById("i16").value;
    proc(4,4,val);
    changeStr(16,val);
}

document.getElementById("olatex").onclick = function() {
    latex();
}

document.getElementById("ocsv").onclick = function() {
    csv();
}

function set(tname, row, col, val){
    document.getElementById(tname).getElementsByTagName("tr")[row].getElementsByTagName("td")[col].innerHTML = val;
    //alert(document.getElementById(tname).getElementsByTagName("tr")[row].getElementsByTagName("td")[col]);
}

ini();

var globalM = ['~','~','~','~','~','~','~','~','~','~','~','~','~','~','~','~'];

function changeStr(n,val){
    globalM[n-1] = val;
    var tmp = '';
    for(var k=0; k<16; k++){
        tmp += globalM[k] + ',';
    }
    tmp = tmp.substring(0,31);
    window.location.hash = tmp;
}

function proc(row,col,digit){
    set("a_t",row,col,0);
    set("b_t",row,col,0);
    set("c_t",row,col,0);
    set("d_t",row,col,0);
    set("e_t",row,col,0);
    set("f_t",row,col,0);
    set("g_t",row,col,0);
    switch(digit) {
        case '0':
            set("a_t",row,col,1);
            set("b_t",row,col,1);
            set("c_t",row,col,1);
            set("d_t",row,col,1);
            set("e_t",row,col,1);
            set("f_t",row,col,1);
            break;
        case '1':
            set("b_t",row,col,1);
            set("c_t",row,col,1);
            break;
        case '2':
            set("a_t",row,col,1);
            set("b_t",row,col,1);
            set("d_t",row,col,1);
            set("e_t",row,col,1);
            set("g_t",row,col,1);
            break;
        case '3':
            set("a_t",row,col,1);
            set("b_t",row,col,1);
            set("c_t",row,col,1);
            set("d_t",row,col,1);
            set("g_t",row,col,1);
            break;
        case '4':
            set("b_t",row,col,1);
            set("c_t",row,col,1);
            set("f_t",row,col,1);
            set("g_t",row,col,1);
            break;
        case '5':
            set("a_t",row,col,1);
            set("c_t",row,col,1);
            set("d_t",row,col,1);
            set("f_t",row,col,1);
            set("g_t",row,col,1);
            break;
        case '6':
            set("a_t",row,col,1);
            set("c_t",row,col,1);
            set("d_t",row,col,1);
            set("e_t",row,col,1);
            set("f_t",row,col,1);
            set("g_t",row,col,1);
            break;
        case '7':
            set("a_t",row,col,1);
            set("b_t",row,col,1);
            set("c_t",row,col,1);
            break;
        case '8':
            set("a_t",row,col,1);
            set("b_t",row,col,1);
            set("c_t",row,col,1);
            set("d_t",row,col,1);
            set("e_t",row,col,1);
            set("f_t",row,col,1);
            set("g_t",row,col,1);
            break;
        case '9':
            set("a_t",row,col,1);
            set("b_t",row,col,1);
            set("c_t",row,col,1);
            set("d_t",row,col,1);
            set("f_t",row,col,1);
            set("g_t",row,col,1);
            break;
        case '-':
            set("a_t",row,col,'-');
            set("b_t",row,col,'-');
            set("c_t",row,col,'-');
            set("d_t",row,col,'-');
            set("e_t",row,col,'-');
            set("f_t",row,col,'-');
            set("g_t",row,col,'-');
            break;
        default:
            set("a_t",row,col,'~');
            set("b_t",row,col,'~');
            set("c_t",row,col,'~');
            set("d_t",row,col,'~');
            set("e_t",row,col,'~');
            set("f_t",row,col,'~');
            set("g_t",row,col,'~');
    }
}

function ini() {
    if(window.location.hash){
        var str = window.location.hash.substring(1);
        if(str.split(',').length != 16)
            return;
        globalM = str.split(',');
        var kk = 1;
        var kj = 1;
        for(var k=0; k<16; k++) {
            //globalM[k] = str[k];
            document.getElementById("i" + String(k+1)).value = globalM[k];
            proc(kk,kj,globalM[k]);
            kj++;
            if(kj == 5) {
                kk++;
                kj = 1;
            }
        }
    }
}

function latex() {
    var tmp = '';
    var tbles = ["main","a","b","c","d","e","f","g"];
    for(var t=0; t<tbles.length; t++){
        var tbl = document.getElementById(tbles[t]+"_t");
        tmp += "\\begin {tabular} {|c|c|c|c|c|} <br>";
        tmp += "\\hline<br>";
        tmp += "\\multicolumn{5}{|c|}{" + tbles[t] + "} \\\\";
        tmp += "\\hline<br>";
        for(var r=0;r<5;r++){
            var row = tbl.getElementsByTagName("tr")[r].getElementsByTagName("td");
            for(var c=0;c<5;c++){
                if((t == 0) && (row[c].getElementsByTagName("input").length > 0))
                    tmp += row[c].getElementsByTagName("input")[0].value + " & ";
                else
                    tmp += "$" + row[c].innerHTML + "$ & ";
            }
                
            tmp = tmp.substring(0,tmp.length-2);
            tmp += "\\\\ \\hline<br>"
        }
        tmp += "\\end {tabular} <br> <br>"
        tmp = tmp.replace(/\<span\ class\=\"overline\"\>/g, '\\overline{');
        tmp = tmp.replace(/\<span\ class=\"lowercase\"\>/g, '_{');
        tmp = tmp.replace(/\<\/span\>/g, '}');
    }
    document.getElementById("latex").innerHTML = "<code>" + tmp + "</code>";
}

function csv() {
    var tmp = '';
    var tbles = ["main_t","a_t","b_t","c_t","d_t","e_t","f_t","g_t"];
    for(var t=0; t<tbles.length; t++){
        var tbl = document.getElementById(tbles[t]);
        for(var r=0;r<5;r++){
            var row = tbl.getElementsByTagName("tr")[r].getElementsByTagName("td");
            for(var c=0;c<5;c++){
                if((t == 0) && (row[c].getElementsByTagName("input").length > 0))
                    tmp += row[c].getElementsByTagName("input")[0].value + ';';
                else
                    tmp += row[c].innerHTML + ';';
            }
            tmp = tmp.substring(0,tmp.length-1)
            tmp += "<br>";
        }
        tmp += ";;;;<br>"
    }
    document.getElementById("latex").innerHTML = "<code>" + tmp + "</code>";
}

//document.getElementById("a_t").getElementsByTagName("tr")[1].getElementsByTagName("td")[1]